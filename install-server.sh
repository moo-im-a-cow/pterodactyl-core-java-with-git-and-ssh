#!\/bin\/ash
apk update
apk add git openssh-client
if [ ! -z "${SSH_KEY}" ]; then
mkdir -p ~/.ssh
cat >~/.ssh/config <<EOL
Host *
    StrictHostKeyChecking no
EOL
SSH_KEY=$(cat <<-END
-----BEGIN RSA PRIVATE KEY-----
${SSH_KEY}
-----END RSA PRIVATE KEY-----
END
)
echo "${SSH_KEY}" > ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa
fi
cd /mnt/server
git clone "${CLONE_URL}" .



#run command.sh
if [ ! -z "${SSH_KEY}" ]; then
  rm -rf ~/.ssh/id_rsa
  mkdir -p ~/.ssh
  echo "${SSH_KEY}" > ~/.ssh/id_rsa
  chmod 600 ~/.ssh/id_rsa
fi
